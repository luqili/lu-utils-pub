package com.luqili.utils.pub.sql;

import com.luqili.utils.pub.base.LuException;

/**
 * 基础异常类型 分页查询异常
 *
 * @author luqili
 */
public class LuDBException extends LuException {
    public LuDBException() {
        super();
    }

    public LuDBException(String message) {
        super(message);
    }

    public LuDBException(String message, Exception e) {
        super(message);
        this.addSuppressed(e);
    }
}

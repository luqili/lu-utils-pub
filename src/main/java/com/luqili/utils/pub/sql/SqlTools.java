package com.luqili.utils.pub.sql;


import cn.hutool.core.util.StrUtil;

/**
 * 处理SQL常用的工具
 */
public class SqlTools {
    public static final String PERCENT = "%";

    /**
     * 为字符串两侧添加%,
     * <p>
     * 字符串为NULL或空字符时返回NULL;
     * <p>
     * 例如：NULL => NULL
     * <p>
     * 例如："" => NULL
     * <p>
     * 例如："a" => "%a%"
     *
     * @param str
     * @return
     */
    public static String handStrLike(String str) {
        if (StrUtil.isNotBlank(str)) {
            if (!str.startsWith(PERCENT)) {
                str = PERCENT + str;
            }
            if (!str.endsWith(PERCENT)) {
                str = str + PERCENT;
            }
            return str;
        }
        return null;
    }

    /**
     * 为字符串左侧添加%,
     * <p>
     * 字符串为NULL或空字符时返回NULL;
     * <p>
     * 例如：NULL => NULL
     * <p>
     * 例如："" => NULL
     * <p>
     * 例如："a" => "%a"
     *
     * @param str
     * @return
     */
    public static String handStrLikeLeft(String str) {
        if (StrUtil.isNotBlank(str)) {
            if (!str.startsWith(PERCENT)) {
                str = PERCENT + str;
            }
            return str;
        }
        return null;
    }

    /**
     * 为字符串右侧添加%,
     * <p>
     * 字符串为NULL或空字符时返回NULL;
     * <p>
     * 例如：NULL => NULL
     * <p>
     * 例如："" => NULL
     * <p>
     * 例如："a" => "a%"
     *
     * @param str
     * @return
     */
    public static String handStrLikeRight(String str) {
        if (StrUtil.isNotBlank(str)) {
            if (!str.endsWith(PERCENT)) {
                str = str + PERCENT;
            }
            return str;
        }
        return null;
    }
}

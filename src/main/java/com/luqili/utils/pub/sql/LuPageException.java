package com.luqili.utils.pub.sql;

import com.luqili.utils.pub.base.LuException;

/**
 * 基础异常类型 分页查询异常
 *
 * @author luqili
 */
public class LuPageException extends LuException {
    public LuPageException() {
        super();
    }

    public LuPageException(String message) {
        super(message);
    }

    public LuPageException(String message, Exception e) {
        super(message);
        this.addSuppressed(e);
    }
}

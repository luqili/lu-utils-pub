package com.luqili.utils.pub.sql;

import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.util.StrUtil;

import java.util.List;

/**
 * 模糊查询、多字段查询条件
 */
public class LuAutoPageSearchKeyParams {
    /**
     * 查询的内容，自动添加%
     */
    private String searchKey;

    /**
     * 查询匹配的字段信息
     */
    private List<String> searchField;

    /**
     * 新建对象
     *
     * @param searchKey    查询的内容，自动添加%
     * @param searchFields 查询匹配的字段信息
     */
    public LuAutoPageSearchKeyParams(String searchKey, String... searchFields) {
        if (StrUtil.isBlank(searchKey)) {
            throw new LuPageException("模糊查询值不能为空");
        }
        if (searchFields == null || searchFields.length < 1) {
            throw new LuPageException("模糊查询字段不能为空");
        }
        this.searchKey = searchKey;
        this.searchField = ListUtil.toList(searchFields);
    }

    public String getSearchKey() {
        return searchKey;
    }

    public void setSearchKey(String searchKey) {
        this.searchKey = searchKey;
    }

    public List<String> getSearchField() {
        return searchField;
    }

    public void setSearchField(List<String> searchField) {
        this.searchField = searchField;
    }
}

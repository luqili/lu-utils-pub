package com.luqili.utils.pub.sql;

import java.util.Map;

/**
 * 处理数据
 */
public interface LuAutoPageHandOne {
    void handOne(Map<String, Object> one);
}

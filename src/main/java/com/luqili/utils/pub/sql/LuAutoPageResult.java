package com.luqili.utils.pub.sql;

import java.util.List;
import java.util.Map;

public class LuAutoPageResult {

    /**
     * 总数
     */
    private int total;

    /**
     * 数据内容
     */
    private List<Map<String, Object>> data;

    /**
     * 统计数据
     */
    private Object totalRow;


    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<Map<String, Object>> getData() {
        return data;
    }

    public void setData(List<Map<String, Object>> data) {
        this.data = data;
    }

    public Object getTotalRow() {
        return totalRow;
    }

    public void setTotalRow(Object totalRow) {
        this.totalRow = totalRow;
    }
}

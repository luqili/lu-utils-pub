package com.luqili.utils.pub.sql;

import org.apache.commons.lang3.StringUtils;

import javax.sql.DataSource;
import java.util.List;

/**
 * 自动分页参数
 */
public class LuAutoPageParams {

    /**
     * 默认关闭链接
     */
    private boolean autoCloseConnection = true;
    /**
     * 可以注入一个数据源
     */
    private DataSource dataSource;
    /**
     * 扩展条件SQL 如：id=? and num=? and (name like ? or remark like ?)
     * 使用占位符时，必须与 extWhereParams 参数对应。
     */
    private String extWhereSql;

    /**
     * 扩展条件SQL参数
     */
    private List<Object> extWhereParams;

    /**
     * 扩展条件SQL插入位置
     */
    private boolean extWhereBefore = true;

    /**
     * 是否统计总数
     */
    private boolean enableTotal = true;

    /**
     * 默认排序字段
     */
    private String defaultSortField;

    /**
     * 扩展统计SQL,统计多个字段的清空，如 count(id),sum(count) as sum
     */
    private String extCountSql;

    /**
     * 处理数据
     */
    private LuAutoPageHandOne luAutoPageHandOne;

    /**
     * 排除查询字段
     */
    private List<String> excludeSelectFields;

    /**
     * 时间段
     */
    private String dateRangeField;
    /**
     * 时间段前置天数
     */
    private Integer dateRangeBeforeDay;
    /**
     * 时间段最大天数
     */
    private Integer dateRangeMaxDay;

    /**
     * 扩展 模糊查询
     */
    private LuAutoPageSearchKeyParams searchKeyParams;

    /**
     * 新建实例
     *
     * @param dataSource 数据源
     */
    public LuAutoPageParams(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * 新建实例
     *
     * @param dataSource   数据源
     * @param keyword      模糊查询的值
     * @param searchFields 查询匹配的字段
     */
    public LuAutoPageParams(DataSource dataSource, String keyword, String... searchFields) {
        this.dataSource = dataSource;
        if (StringUtils.isNotBlank(keyword)) {
            this.searchKeyParams = new LuAutoPageSearchKeyParams(keyword, searchFields);
        }
    }

    public String getExtWhereSql() {
        return extWhereSql;
    }

    public void setExtWhereSql(String extWhereSql) {
        this.extWhereSql = extWhereSql;
    }

    public List<Object> getExtWhereParams() {
        return extWhereParams;
    }

    public void setExtWhereParams(List<Object> extWhereParams) {
        this.extWhereParams = extWhereParams;
    }

    public boolean isExtWhereBefore() {
        return extWhereBefore;
    }

    public void setExtWhereBefore(boolean extWhereBefore) {
        this.extWhereBefore = extWhereBefore;
    }

    public boolean isEnableTotal() {
        return enableTotal;
    }

    public void setEnableTotal(boolean enableTotal) {
        this.enableTotal = enableTotal;
    }

    public String getDefaultSortField() {
        return defaultSortField;
    }

    public void setDefaultSortField(String defaultSortField) {
        this.defaultSortField = defaultSortField;
    }

    public String getExtCountSql() {
        return extCountSql;
    }

    public void setExtCountSql(String extCountSql) {
        this.extCountSql = extCountSql;
    }

    public LuAutoPageHandOne getLuAutoPageHandOne() {
        return luAutoPageHandOne;
    }

    public void setLuAutoPageHandOne(LuAutoPageHandOne luAutoPageHandOne) {
        this.luAutoPageHandOne = luAutoPageHandOne;
    }

    public List<String> getExcludeSelectFields() {
        return excludeSelectFields;
    }

    public void setExcludeSelectFields(List<String> excludeSelectFields) {
        this.excludeSelectFields = excludeSelectFields;
    }

    public String getDateRangeField() {
        return dateRangeField;
    }

    public void setDateRangeField(String dateRangeField) {
        this.dateRangeField = dateRangeField;
    }

    public Integer getDateRangeBeforeDay() {
        return dateRangeBeforeDay;
    }

    public void setDateRangeBeforeDay(Integer dateRangeBeforeDay) {
        this.dateRangeBeforeDay = dateRangeBeforeDay;
    }

    public Integer getDateRangeMaxDay() {
        return dateRangeMaxDay;
    }

    public void setDateRangeMaxDay(Integer dateRangeMaxDay) {
        this.dateRangeMaxDay = dateRangeMaxDay;
    }

    public DataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public boolean isAutoCloseConnection() {
        return autoCloseConnection;
    }

    public void setAutoCloseConnection(boolean autoCloseConnection) {
        this.autoCloseConnection = autoCloseConnection;
    }

    public LuAutoPageSearchKeyParams getSearchKeyParams() {
        return searchKeyParams;
    }

    public void setSearchKeyParams(LuAutoPageSearchKeyParams searchKeyParams) {
        this.searchKeyParams = searchKeyParams;
    }
}

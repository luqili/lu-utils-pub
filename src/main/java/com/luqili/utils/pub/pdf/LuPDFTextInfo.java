package com.luqili.utils.pub.pdf;

import org.apache.pdfbox.pdmodel.font.PDFont;

import java.awt.*;

public class LuPDFTextInfo {
    /**
     * 内容
     */
    private String value;
    /**
     * 颜色
     */
    private Color color;
    /**
     * 字体大小
     */
    private float size;
    /**
     * 选用字体
     */
    private PDFont font;
    /**
     * 权重
     */
    private Integer weight;


    public LuPDFTextInfo() {
    }

    public LuPDFTextInfo(String value, PDFont font, float size) {
        this.value = value;
        this.font = font;
        this.size = size;
    }

    public LuPDFTextInfo(String value, PDFont font, float size, Integer weight) {
        this.value = value;
        this.font = font;
        this.size = size;
        this.weight = weight;
    }

    public LuPDFTextInfo(String value, PDFont font, float size, Integer weight, Color color) {
        this.value = value;
        this.font = font;
        this.size = size;
        this.weight = weight;
        this.color = color;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public float getSize() {
        return size;
    }

    public void setSize(float size) {
        this.size = size;
    }

    public PDFont getFont() {
        return font;
    }

    public void setFont(PDFont font) {
        this.font = font;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }
}

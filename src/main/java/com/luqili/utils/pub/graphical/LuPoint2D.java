package com.luqili.utils.pub.graphical;

/**
 * 2D坐标点
 */
public class LuPoint2D {
    public float x, y;

    public LuPoint2D() {
    }

    public LuPoint2D(float x) {
        this.x = x;
    }

    public LuPoint2D(float x, float y) {
        this.x = x;
        this.y = y;
    }
}

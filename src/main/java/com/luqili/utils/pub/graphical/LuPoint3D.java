package com.luqili.utils.pub.graphical;

/**
 * 3D坐标点
 *
 * @author luqili
 */
public class LuPoint3D {
    public float x, y, z;

    public LuPoint3D() {
    }

    public LuPoint3D(float x) {
        this.x = x;
    }

    public LuPoint3D(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public LuPoint3D(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
}

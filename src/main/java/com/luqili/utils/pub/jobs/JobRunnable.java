package com.luqili.utils.pub.jobs;

/**
 * 扩展停止功能
 *
 * @author luqili
 */
public abstract class JobRunnable implements Runnable {
    protected boolean jobRun = true;

    /**
     * 停止后不允许再服用该Bean了
     */
    public void endJob() {
        jobRun = false;
    }
}

package com.luqili.utils.pub.httpclient;

import com.luqili.utils.pub.string.CharsetUtil;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.TrustStrategy;
import org.apache.http.util.EntityUtils;

import javax.net.ssl.SSLContext;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

public class HttpClientTool {
    private static CloseableHttpClient HTTP_CLIENT = null;
    private static CloseableHttpClient HTTPS_CLIENT = null;

    static {
        HTTP_CLIENT = HttpClientBuilder.create().build();
        SSLContext sslContext = null;
        try {
            sslContext = new SSLContextBuilder().loadTrustMaterial(null, new TrustStrategy() {
                @Override
                public boolean isTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                    return true;
                }
            }).build();
        } catch (Exception e) {
            e.printStackTrace();
        }
        HTTPS_CLIENT = HttpClientBuilder.create().setSSLContext(sslContext).build();
    }

    /**
     * Post请求
     *
     * @param serverUrl
     * @param requestJson
     * @return
     */
    public static String requestPost(String serverUrl, String requestJson) {
        CloseableHttpClient httpClient = HttpClientTool.HTTP_CLIENT;
        if (serverUrl.startsWith("https")) {
            httpClient = HttpClientTool.HTTPS_CLIENT;
        }

        HttpPost post = new HttpPost(serverUrl);
        StringEntity entity = new StringEntity(requestJson, "utf-8");
        post.setEntity(entity);
        CloseableHttpResponse response = null;
        try {
            response = httpClient.execute(post);
            HttpEntity result = response.getEntity();
            String h = EntityUtils.toString(result);
            return h;
        } catch (Exception e) {
            throw new RuntimeException("网络请求失败:" + e.getMessage());
        } finally {
            HttpClientUtils.closeQuietly(response);
        }
    }

    /**
     * Get请求
     *
     * @param serverUrl
     * @return
     */
    public static String requestGet(String serverUrl, Integer timeOut) {

        CloseableHttpClient httpClient = HttpClientTool.HTTP_CLIENT;
//    if (serverUrl.startsWith("https")) {
//      httpClient = HttpClientTool.HTTPS_CLIENT;
//    }
        RequestConfig config = RequestConfig.custom().setConnectionRequestTimeout(timeOut).setConnectTimeout(timeOut).setSocketTimeout(timeOut).build();

        HttpGet get = new HttpGet(serverUrl);
        get.setConfig(config);
        CloseableHttpResponse response = null;
        try {
            response = httpClient.execute(get);
            HttpEntity result = response.getEntity();
            String h = EntityUtils.toString(result, CharsetUtil.UTF_8);
            return h;
        } catch (Exception e) {
            throw new RuntimeException("网络请求失败:" + e.getMessage());
        } finally {
            HttpClientUtils.closeQuietly(response);
        }
    }
}

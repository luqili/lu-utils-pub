package com.luqili.utils.pub.crypto;

public class AESException extends RuntimeException {
    /**
     * 秘钥异常
     */
    public static final int TYPE_PRIVATE_KEY = 1;
    /**
     * 解密异常
     */
    public static final int TYPE_DECRYPT = 2;
    /**
     *
     */
    private static final long serialVersionUID = -6006428462087266184L;
    private int type;

    public AESException(int type, Throwable cause) {
        this.type = type;
        if (cause != null) {
            this.initCause(cause);
        }
    }

    /**
     * 创建秘钥异常
     *
     * @return
     */
    public static AESException createPrivateKey(Throwable cause) {
        return new AESException(TYPE_PRIVATE_KEY, cause);
    }

    /**
     * 创建解密异常
     *
     * @return
     */
    public static AESException createDecrypt(Throwable cause) {
        return new AESException(TYPE_DECRYPT, cause);
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

}

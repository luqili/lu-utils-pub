package com.luqili.utils.pub.string;

import org.apache.commons.lang3.StringUtils;

/**
 * 字符串工具类扩展
 * <li>对StringUtils的补充
 *
 * @author luqili
 */
public class StringUtilsExt {

    /**
     * 验证前缀样式是否一致
     *
     * <pre>
     * StringUtils.startsWithStyle(null, null)      = true
     * StringUtils.startsWithStyle(null, "")      = true
     * StringUtils.startsWithStyle("abcdef", "")      = true
     * StringUtils.startsWithStyle("abcdef", null)  = true
     * StringUtils.startsWithStyle("abcdef", "abc") = true
     * StringUtils.startsWithStyle("abc", "abc")     = true
     * StringUtils.startsWithStyle("abcd", "abc")     = true
     * StringUtils.startsWithStyle("abcd", "a*c")     = true
     *
     * StringUtils.startsWithStyle("abbd", "a*c")     = false
     * StringUtils.startsWithStyle(null, "abc")     = false
     * StringUtils.startsWithStyle("ab", "abc")     = false
     * StringUtils.startsWithStyle("ABCDEF", "abc") = false
     * </pre>
     *
     * @param value
     * @param style 解析 * 为单个任意字符
     * @return
     */
    public static boolean startsWithStyle(String value, String style) {
        if (StringUtils.isBlank(style)) {
            return true;
        }
        if (StringUtils.isBlank(value)) {
            return false;
        }

        if (style.length() > value.length()) {
            return false;
        }
        char[] v1 = value.toCharArray();
        char[] s1 = style.toCharArray();
        for (int i = 0; i < s1.length; i++) {
            if (v1[i] != s1[i] && s1[i] != '*') {
                return false;
            }
        }
        return true;
    }

    /**
     * 字符串隐藏中间字符
     *
     * <pre>
     * String t1 = StringUtils.hideMiddleCharacter(null, 1, 2, "*", 3) = "***";
     * String t2 = StringUtils.hideMiddleCharacter("a", 1, 2, "*", 3) = "a***a";
     * String t3 = StringUtils.hideMiddleCharacter("ab", 1, 2, "*", 3) = "a***ab";
     * String t4 = StringUtils.hideMiddleCharacter("abc", 1, 2, "*", 3) = "a***bc";
     * String t5 = StringUtils.hideMiddleCharacter("abcd", 1, 2, "*", 3) = "a***abc";
     * </pre>
     *
     * @param src          字符串
     * @param leftSize     左侧显示数量
     * @param leftSize     右侧显示数量
     * @param replace      替换字符
     * @param showHideSize 显示隐藏字符数量
     * @return
     */
    public static String hideMiddleCharacter(String src, Integer leftSize, Integer rightSize, String replace, Integer showHideSize) {
        StringBuffer result = new StringBuffer();
        if (StringUtils.isBlank(src)) {
            src = "";
        }
        if (leftSize == null || leftSize < 0) {
            leftSize = 2;
        }
        if (rightSize == null || rightSize < 0) {
            rightSize = 2;
        }
        if (StringUtils.isBlank(replace)) {
            replace = "*";
        }
        if (showHideSize == null || showHideSize < 0) {
            rightSize = 4;
        }
        result.append(StringUtils.left(src, leftSize));
        result.append(StringUtils.repeat(replace, showHideSize));
        result.append(StringUtils.right(src, rightSize));
        return result.toString();
    }

    /**
     * 查询一样的前缀部分
     *
     * @param str1
     * @param str2
     * @return
     */
    public static String samePrefix(String str1, String str2) {
        if (StringUtils.isBlank(str1) || StringUtils.isBlank(str2)) {
            return "";
        }
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < str1.length() && i < str2.length(); i++) {
            if (str1.charAt(i) == str2.charAt(i)) {
                sb.append(str1.charAt(i));
            } else {
                break;
            }
        }
        return sb.toString();
    }
}

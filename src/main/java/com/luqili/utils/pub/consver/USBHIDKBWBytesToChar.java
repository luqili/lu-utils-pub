package com.luqili.utils.pub.consver;

import java.util.HashMap;

/**
 * 二进制数组转换为字符串 部分 支持 Base64编码部分
 *
 * @author luqili
 */
public class USBHIDKBWBytesToChar {

    private static final HashMap<Integer, Character> TAB0 = new HashMap<Integer, Character>();
    private static final HashMap<Integer, Character> TAB2 = new HashMap<Integer, Character>();

    static {
        TAB0.put(0x04, 'a');
        TAB0.put(0x05, 'b');
        TAB0.put(0x06, 'c');
        TAB0.put(0x07, 'd');
        TAB0.put(0x08, 'e');
        TAB0.put(0x09, 'f');
        TAB0.put(0x0A, 'g');
        TAB0.put(0x0B, 'h');
        TAB0.put(0x0C, 'i');
        TAB0.put(0x0D, 'j');
        TAB0.put(0x0E, 'k');
        TAB0.put(0x0F, 'l');
        TAB0.put(0x10, 'm');
        TAB0.put(0x11, 'n');
        TAB0.put(0x12, 'o');
        TAB0.put(0x13, 'p');
        TAB0.put(0x14, 'q');
        TAB0.put(0x15, 'r');
        TAB0.put(0x16, 's');
        TAB0.put(0x17, 't');
        TAB0.put(0x18, 'u');
        TAB0.put(0x19, 'v');
        TAB0.put(0x1A, 'w');
        TAB0.put(0x1B, 'x');
        TAB0.put(0x1C, 'y');
        TAB0.put(0x1D, 'z');
        TAB0.put(0x1E, '1');
        TAB0.put(0x1F, '2');
        TAB0.put(0x20, '3');
        TAB0.put(0x21, '4');
        TAB0.put(0x22, '5');
        TAB0.put(0x23, '6');
        TAB0.put(0x24, '7');
        TAB0.put(0x25, '8');
        TAB0.put(0x26, '9');
        TAB0.put(0x27, '0');
        TAB0.put(0x28, '\n');// 回车
        TAB0.put(0x2C, ' ');// 空格 Spacebar
        TAB0.put(0x2D, '-');
        TAB0.put(0x2E, '=');
        TAB0.put(0x2F, '[');
        TAB0.put(0x30, ']');
        TAB0.put(0x31, '\\');
        TAB0.put(0x32, '`');// ？
        TAB0.put(0x33, ';');
        TAB0.put(0x34, '\'');
        // TAB0.put(0x35, ''); ？
        TAB0.put(0x36, ',');
        TAB0.put(0x37, '.');
        TAB0.put(0x38, '/');

        TAB2.put(0x04, 'A');
        TAB2.put(0x05, 'B');
        TAB2.put(0x06, 'C');
        TAB2.put(0x07, 'D');
        TAB2.put(0x08, 'E');
        TAB2.put(0x09, 'F');
        TAB2.put(0x0A, 'G');
        TAB2.put(0x0B, 'H');
        TAB2.put(0x0C, 'I');
        TAB2.put(0x0D, 'J');
        TAB2.put(0x0E, 'K');
        TAB2.put(0x0F, 'L');
        TAB2.put(0x10, 'M');
        TAB2.put(0x11, 'N');
        TAB2.put(0x12, 'O');
        TAB2.put(0x13, 'P');
        TAB2.put(0x14, 'Q');
        TAB2.put(0x15, 'R');
        TAB2.put(0x16, 'S');
        TAB2.put(0x17, 'T');
        TAB2.put(0x18, 'U');
        TAB2.put(0x19, 'V');
        TAB2.put(0x1A, 'W');
        TAB2.put(0x1B, 'X');
        TAB2.put(0x1C, 'Y');
        TAB2.put(0x1D, 'Z');
        TAB2.put(0x1E, '!');
        TAB2.put(0x1F, '@');
        TAB2.put(0x20, '#');
        TAB2.put(0x21, '$');
        TAB2.put(0x22, '%');
        TAB2.put(0x23, '^');
        TAB2.put(0x24, '&');
        TAB2.put(0x25, '*');
        TAB2.put(0x26, '(');
        TAB2.put(0x27, ')');
        TAB2.put(0x28, '\n');// 回车
        TAB2.put(0x2D, '_');
        TAB2.put(0x2E, '+');
        TAB2.put(0x2F, '{');
        TAB2.put(0x30, '}');
        TAB2.put(0x31, '|');
        TAB2.put(0x32, '~');// ?
        TAB2.put(0x33, ':');
        TAB2.put(0x34, '"');
        // TAB2.put(0x35, ''); ？
        TAB2.put(0x36, '<');
        TAB2.put(0x37, '>');
        TAB2.put(0x38, '?');
    }

    public static boolean testBase64Char() {
        char[] toBase64 = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
            'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/', '-', '_', '{', '}', '"', '\''};
        for (char c : toBase64) {
            if (!TAB0.values().contains(c) && !TAB2.values().contains(c)) {
                throw new RuntimeException("不支持字符：" + c);
            }
        }
        return true;
    }

    /**
     * 转换为键盘字符
     *
     * @param src 8位
     * @return
     */
    public static Character consver(byte[] src) {
        if (src[2] == 0) {
            return null;
        }
        if (src[0] == 0) {
            return TAB0.get((int) src[2]);
        } else if (src[0] == 2) {
            return TAB2.get((int) src[2]);
        } else {
            return null;
        }
    }

}

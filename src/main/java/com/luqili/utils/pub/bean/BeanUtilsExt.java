package com.luqili.utils.pub.bean;

import org.apache.commons.beanutils.BeanUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;

/**
 * Bean工具扩展
 *
 * @author luqili
 */
public class BeanUtilsExt {

    /**
     * 安静的经Bean转换为Map<String,String>
     *
     * @param bean
     * @return
     */
    public static Map<String, String> describeQuietly(Object bean) {
        try {
            return BeanUtils.describe(bean);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 单层对象转Map
     * <li>不包含空值
     * <li>不包含静态常量
     *
     * @param obj
     * @return
     * @throws Exception
     * @author luqili
     * @version 2018年5月29日
     */
    public static Map<String, Object> objectToMap(Object obj) {
        if (obj == null) {
            return null;
        }
        Map<String, Object> map = new HashMap<String, Object>();
        Field[] declaredFields = obj.getClass().getDeclaredFields();
        for (Field field : declaredFields) {
            field.setAccessible(true);
            try {
                Object val = field.get(obj);
                if (val != null) {
                    int r = field.getModifiers();
                    if ((Modifier.FINAL & r) != Modifier.FINAL) {
                        map.put(field.getName(), val);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return map;
    }
}

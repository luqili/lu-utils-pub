package com.luqili.utils.pub.link;

import cn.hutool.core.util.ObjectUtil;

import java.io.Serializable;

/**
 * 三个实体Bean
 *
 * @param <F>
 * @param <S>
 * @param <T>
 * @author 46155
 */
public class Three<F, S, T> implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 6802018538093390685L;
    private F first;
    private S second;
    private T third;

    public Three(F first, S second, T third) {
        this.first = first;
        this.second = second;
        this.third = third;
    }

    public F getFirst() {
        return first;
    }

    public void setFirst(F first) {
        this.first = first;
    }

    public S getSecond() {
        return second;
    }

    public void setSecond(S second) {
        this.second = second;
    }

    public T getThird() {
        return third;
    }

    public void setThird(T third) {
        this.third = third;
    }

    @Override
    public String toString() {
        return first + "=" + second + "=" + third;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof Three) {
            Three ot = (Three) obj;

            if (ObjectUtil.equals(this.first, ot.first)
                && ObjectUtil.equals(this.second, ot.second)
                && ObjectUtil.equals(this.third, ot.third)
            ) {
                return true;
            }
        }
        return false;
    }
}

package com.luqili.utils.pub.consts;

/**
 * 结果
 *
 * @author luqili
 */
public class LuConstResultStr implements LuConstRoot<String> {

    /**
     * 成功
     */
    public static final String SUCCESS = "SUCCESS";
    /**
     * 失败
     */
    public static final String FAIL = "FAIL";

    /**
     * 单例获取
     *
     * @return
     */
    public static LuConstResultStr getInstance() {
        return LuConstTools.getInstance(LuConstResultStr.class);
    }

    /**
     * 获取分类名称
     *
     * @return 类型名
     */
    @Override
    public String getCategoryName() {
        return "字符串结果";
    }

    @Override
    public String getName(String val) {
        if (val != null) {
            switch (val) {
                case SUCCESS:
                    return "成功";
                case FAIL:
                    return "失败";
                default:
                    break;
            }
        }
        return "";
    }

    @Override
    public String getColor(String val) {
        if (val != null) {
            switch (val) {
                case SUCCESS:
                    return LuConstColorHex.FOREST_GREEN;
                case FAIL:
                    return LuConstColorHex.GRAY;
                default:
                    return LuConstColorHex.RED;
            }
        }
        return LuConstColorHex.RED;
    }

}

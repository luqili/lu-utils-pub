package com.luqili.utils.pub.consts;

/**
 * 结果集
 *
 * @author luqili
 */
public class LuConstResult implements LuConstRoot<Integer> {

    /**
     * 成功
     */
    public static final int SUCCESS = 1;
    /**
     * 失败
     */
    public static final int FAIL = 4;

    /**
     * 单例获取
     *
     * @return
     */
    public static LuConstResult getInstance() {
        return LuConstTools.getInstance(LuConstResult.class);
    }

    /**
     * 获取分类名称
     *
     * @return 类型名
     */
    @Override
    public String getCategoryName() {
        return "数字结果";
    }

    @Override
    public String getName(Integer val) {
        if (val != null) {
            switch (val) {
                case SUCCESS:
                    return "成功";
                case FAIL:
                    return "失败";
                default:
                    break;
            }
        }
        return "";
    }

    @Override
    public String getColor(Integer val) {
        if (val != null) {
            switch (val) {
                case SUCCESS:
                    return LuConstColorHex.FOREST_GREEN;
                case FAIL:
                    return LuConstColorHex.GRAY;
                default:
                    return LuConstColorHex.RED;
            }
        }
        return LuConstColorHex.RED;
    }

}

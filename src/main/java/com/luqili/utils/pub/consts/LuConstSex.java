package com.luqili.utils.pub.consts;

import org.apache.commons.lang3.StringUtils;

/**
 * 常量 - 性别
 *
 * @author luqili
 */
public class LuConstSex implements LuConstRoot<Integer> {

    /**
     * 未知|保密
     */
    public static final int UNKNOWN = 0;
    /**
     * 男
     */
    public static final int MAN = 1;
    /**
     * 女
     */
    public static final int WOMAN = 2;

    /**
     * 单例获取
     *
     * @return
     */
    public static LuConstSex getInstance() {
        return LuConstTools.getInstance(LuConstSex.class);
    }

    /**
     * 获取分类名称
     *
     * @return 类型名
     */
    @Override
    public String getCategoryName() {
        return "性别";
    }

    @Override
    public String getName(Integer val) {
        if (val == null) {
            return "";
        }
        switch (val) {
            case UNKNOWN:
                return "保密";
            case MAN:
                return "男";
            case WOMAN:
                return "女";
            default:
                return "";
        }

    }

    private static final String[] CHARTS_SEX_WOMAN = new String[]{"女", "girl", "woman"};
    private static final String[] CHARTS_SEX_MAN = new String[]{"男", "boy", "man"};

    @Override
    public Integer getValue(String sex) {
        if (StringUtils.isNotBlank(sex)) {
            if (StringUtils.isNumeric(sex)) {
                int s=Integer.parseInt(sex);
                switch (s){
                    case MAN:
                    case WOMAN:
                        return s;
                    default:
                        return UNKNOWN;
                }
            }
            String sexLower = sex.toLowerCase();
            if (StringUtils.containsAny(sexLower, CHARTS_SEX_WOMAN)) {
                return WOMAN;
            }
            if (StringUtils.containsAny(sexLower, CHARTS_SEX_MAN)) {
                return MAN;
            }
        }
        return UNKNOWN;
    }

    @Override
    public String getColor(Integer val) {
        if (val != null) {
            switch (val) {
                case UNKNOWN:
                    return LuConstColorHex.GRAY;
                case MAN:
                    return LuConstColorHex.BLUE;
                case WOMAN:
                    return LuConstColorHex.PINK;
                default:
            }
        }
        return "";

    }
}

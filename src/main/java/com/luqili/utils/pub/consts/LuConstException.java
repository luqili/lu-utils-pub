package com.luqili.utils.pub.consts;

import cn.hutool.poi.excel.ExcelUtil;
import com.luqili.utils.pub.base.LuException;

/**
 * 常量处理异常
 *
 * @author luqili
 */
public class LuConstException extends LuException {
    public LuConstException() {

    }

    public LuConstException(String message) {
        super(message);
    }

    public LuConstException(String message, Exception e) {
        super(message, e);
    }
}

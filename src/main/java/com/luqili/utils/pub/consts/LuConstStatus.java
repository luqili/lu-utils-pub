package com.luqili.utils.pub.consts;


import org.apache.commons.lang3.StringUtils;

/**
 * 状态常量
 *
 * @author luqili
 */
public class LuConstStatus implements LuConstRoot<Integer> {
    /**
     * 失效
     */
    public static final int INVALID = 0;
    /**
     * 不允许对外使用，必须使用Private修饰，避免解析异常
     */
    private static final String NAME_INVALID = "失效";

    /**
     * 有效
     */
    public static final int VALID = 1;
    private static final String NAME_VALID = "有效";
    /**
     * 限制
     */
    public static final int LIMIT = 2;
    private static final String NAME_LIMIT = "限制";
    /**
     * 异常
     */
    public static final int EXCEPTION = 5;
    private static final String NAME_EXCEPTION = "异常";

    /**
     * 单例获取
     *
     * @return
     */
    public static LuConstStatus getInstance() {
        return LuConstTools.getInstance(LuConstStatus.class);
    }

    @Override
    public String getCategoryName() {
        return "状态";
    }

    @Override
    public String getName(Integer val) {
        if (val != null) {
            switch (val) {
                case INVALID:
                    return NAME_INVALID;
                case VALID:
                    return NAME_VALID;
                case LIMIT:
                    return NAME_LIMIT;
                case EXCEPTION:
                    return NAME_EXCEPTION;
                default:
            }
        }
        return "";
    }

    @Override
    public Integer getValue(String name) {
        if (StringUtils.isNotBlank(name)) {
            switch (name) {
                case NAME_INVALID:
                case "无效":
                    return INVALID;
                case NAME_VALID:
                    return VALID;
                case NAME_LIMIT:
                    return LIMIT;
                case NAME_EXCEPTION:
                    return EXCEPTION;
                default:
            }
        }
        return null;
    }

    @Override
    public String getColor(Integer val) {
        if (val != null) {
            switch (val) {
                case INVALID:
                    return LuConstColorHex.GRAY;
                case VALID:
                    return LuConstColorHex.GREEN;
                case LIMIT:
                    return LuConstColorHex.ORANGE;
                case EXCEPTION:
                    return LuConstColorHex.RED;
                default:
            }
        }
        return "";
    }
}

package com.luqili.utils.pub.consts;

/**
 * 十六进制颜色
 *
 * @author luqili
 */
public interface LuConstColorHex {
    /**
     * <p style="font-size: 20px;color:#04BE02;">微信绿色 #04BE02</p>
     */
    String WEIXIN = "#04BE02";

    /**
     * <p style="font-size: 20px;color:#027AFF;">支付宝蓝色 #027AFF</p>
     */
    String ALIPAY = "#027AFF";

    /**
     * <p style="font-size: 20px;color:#004683;">银联商务蓝色 #004683</p>
     */
    String CHINAUMS = "#004683";

    /**
     * <p style="font-size: 20px;color:#FF0000;">Red 红色 #FF0000</p>
     */
    String RED = "#FF0000";
    /**
     * <p style="font-size: 20px;color:#8B0000;"> 深红色 #8B0000</p>
     */
    String DARK_RED = "#8B0000";
    /**
     * <p style="font-size: 20px;color:#FFC0CB;">Pink 粉红 #FFC0CB</p>
     */
    String PINK = "#FFC0CB";
    /**
     * <p style="font-size: 20px;color:#FF00FF;"> Magenta 洋红(玫瑰红) #FF00FF</p>
     */
    String MAGENTA = "#FF00FF";

    /**
     * <p style="font-size: 20px;color:#FFA500;">橙色 #FFA500</p>
     */
    String ORANGE = "#FFA500";
    /**
     * <p style="font-size: 20px;color:#FF4500;">橙红色 #FF4500</p>
     */
    String ORANGE_RED = "#FF4500";

    /**
     * <p style="font-size: 20px;color:#FFFF00;" >黄色 #FFFF00</p>
     */
    String YELLOW = "#FFFF00";


    /**
     * <p style="font-size: 20px;color:#008000;">绿色 #008000</p>
     */
    String GREEN = "#008000";
    /**
     * <p style="font-size: 20px;color:#00FF7F;">SpringGreen 春绿色 #00FF7F</p>
     */
    String SPRING_GREEN = "#00FF7F";
    /**
     * <p style="font-size: 20px;color:#228B22;">ForestGreen 森林绿 #228B22</p>
     */
    String FOREST_GREEN = "#228B22";
    /**
     * <p style="font-size: 20px;color:#006400;">暗绿色 #006400</p>
     */
    String DARK_GREEN = "#006400";
    /**
     * <p style="font-size: 20px;color:#00CED1;">DarkTurquoise 暗绿宝石 #00CED1</p>
     */
    String DARK_TURQUOISE = "#00CED1";

    /**
     * <p style="font-size: 20px;color:#008080;">青色 #008080</p>
     */
    String TEAL = "#008080";

    /**
     * <p style="font-size: 20px;color:#0000FF;">BLUE 纯蓝 #0000FF</p>
     */
    String BLUE = "#0000FF";
    /**
     * <p style="font-size: 20px;color:#87CEEB;">Sky Blue 天蓝色 #87CEEB</p>
     */
    String SKY_BLUE = "#87CEEB";
    /**
     * <p style="font-size: 20px;color:#00BFFF;">Deep Sky Blue 深天蓝 #00BFFF</p>
     */
    String DEEP_SKY_BLUE = "#00BFFF";
    /**
     * <p style="font-size: 20px;color:#4682B4;">SteelBLUE 钢蓝/铁青 #4682B4</p>
     */
    String STEEL_BLUE = "#4682B4";
    /**
     * <p style="font-size: 20px;color:#4169E1;">RoyalBLUE 皇家蓝/宝蓝 #4169E1</p>
     */
    String ROYAL_BLUE = "#4169E1";


    /**
     * <p style="font-size: 20px;color:#808080;"> 灰色 #808080 </p>
     */
    String GRAY = "#808080";
    /**
     * <p style="font-size: 20px;color:#303030;"> 灰色3 #303030 </p>
     */
    String GRAY_3 = "#303030";
    /**
     * <p style="font-size: 20px;color:#606060;"> 灰色5 #505050 </p>
     */
    String GRAY_5 = "#505050";
    /**
     * <p style="font-size: 20px;color:#606060;"> 灰色6 #606060 </p>
     */
    String GRAY_6 = "#606060";
    /**
     * <p style="font-size: 20px;color:#DimGray;"> 暗淡的灰色 #696969 </p>
     */
    String DIM_GRAY = "#696969";
    /**
     * <p style="font-size: 20px;color:#808080;"> 灰色8 #808080 </p>
     */
    String GRAY_8 = "#808080";
    /**
     * <p style="font-size: 20px;color:#909090;"> 灰色9 #909090 </p>
     */
    String GRAY_9 = "#909090";
    /**
     * <p style="font-size: 20px;color:#A0A0A0;"> 灰色A #A0A0A0 </p>
     */
    String GRAY_A = "#A0A0A0";
    /**
     * <p style="font-size: 20px;color:#B0B0B0;"> 灰色B #B0B0B0 </p>
     */
    String GRAY_B = "#B0B0B0";
    /**
     * <p style="font-size: 20px;color:#C0C0C0;"> 灰色C #C0C0C0 </p>
     */
    String GRAY_C = "#C0C0C0";
    /**
     * <p style="font-size: 20px;color:#D0D0D0;"> 灰色C #D0D0D0 </p>
     */
    String GRAY_D = "#D0D0D0";
    /**
     * <p style="font-size: 20px;color:#E0E0E0;"> 灰色E #E0E0E0 </p>
     */
    String GRAY_E = "#E0E0E0";

    /**
     * <p style="font-size: 20px;color:#000000;">纯黑 #000000</p>
     */
    String BLACK = "#000000";

    /**
     * 纯白 <p style="font-size: 20px;color:#FFFFFF;">纯白 #FFFFFF </p>
     */
    String WHITE = "#FFFFFF";
}

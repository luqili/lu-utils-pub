package com.luqili.utils.pub.consts;

import java.util.HashSet;
import java.util.Set;

/**
 * 状态常量
 */
public class LuConstBoolean implements LuConstRoot<Boolean> {

    /**
     * 获取所有常量
     *
     * @return 常量数组
     */
    @Override
    public Set<Boolean> getValues() {
        Set<Boolean> set = new HashSet<>();
        set.add(true);
        set.add(false);
        return set;
    }

    /**
     * 单例获取
     *
     * @return
     */
    public static LuConstBoolean getInstance() {
        return LuConstTools.getInstance(LuConstBoolean.class);
    }

    @Override
    public String getCategoryName() {
        return "是否";
    }

    @Override
    public String getName(Boolean val) {
        if (val != null) {
            return val ? "是" : "否";
        }
        return "";
    }

    @Override
    public String getColor(Boolean val) {
        if (val != null) {
            return val ? LuConstColorHex.GREEN : LuConstColorHex.GRAY;
        }
        return "";
    }
}

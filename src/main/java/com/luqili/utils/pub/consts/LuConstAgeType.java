package com.luqili.utils.pub.consts;

/**
 * 常量 - 性别
 *
 * @author luqili
 */
public class LuConstAgeType implements LuConstRoot<Integer> {

    /**
     * 儿童
     */
    public static final int CHILD = 1;
    /**
     * 成人
     */
    public static final int ADULT = 2;
    /**
     * 老人
     */
    public static final int OLD_MAN = 3;

    /**
     * 单例获取
     *
     * @return
     */
    public static LuConstAgeType getInstance() {
        return LuConstTools.getInstance(LuConstAgeType.class);
    }

    /**
     * 获取分类名称
     *
     * @return 类型名
     */
    @Override
    public String getCategoryName() {
        return "年龄类型";
    }

    @Override
    public String getName(Integer val) {
        if (val != null) {
            switch (val) {
                case CHILD:
                    return "未成年";
                case ADULT:
                    return "成人";
                case OLD_MAN:
                    return "老人";
                default:
            }
        }
        return "";
    }

}

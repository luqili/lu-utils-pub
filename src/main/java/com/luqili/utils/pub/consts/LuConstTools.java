package com.luqili.utils.pub.consts;

import java.util.HashMap;
import java.util.Map;

/**
 * 常量通用工具类
 *
 * @author luqili
 */
public class LuConstTools {

    private static final Map<Class, Object> instances = new HashMap<>();

    /**
     * 获取指定的常量实例
     *
     * @param aClass
     * @param <T>
     * @return
     */
    public static <T extends LuConstRoot> T getInstance(Class<T> aClass) {
        T ins = instances.containsKey(aClass) ? (T) instances.get(aClass) : null;
        if (ins == null) {
            try {
                ins = aClass.newInstance();
                instances.put(aClass, ins);
            } catch (Exception e) {
                throw new LuConstException("常量类型实例化失败，请检测构造方法:" + aClass.getSimpleName(), e);
            }
        }
        return ins;
    }

}

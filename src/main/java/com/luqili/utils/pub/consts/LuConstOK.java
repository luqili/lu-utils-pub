package com.luqili.utils.pub.consts;

/**
 * 状态常量
 */
public class LuConstOK implements LuConstRoot<Integer> {
    /**
     * 无效
     */
    public static final int NO = 0;
    /**
     * 有效
     */
    public static final int OK = 1;

    /**
     * 单例获取
     *
     * @return
     */
    public static LuConstOK getInstance() {
        return LuConstTools.getInstance(LuConstOK.class);
    }

    @Override
    public String getCategoryName() {
        return "数字是否";
    }

    @Override
    public String getName(Integer val) {
        if (val != null) {
            switch (val) {
                case NO:
                    return "否";
                case OK:
                    return "是";
                default:
            }
        }
        return "";
    }

    @Override
    public String getColor(Integer val) {
        if (val != null) {
            switch (val) {
                case NO:
                    return LuConstColorHex.GRAY;
                case OK:
                    return LuConstColorHex.GREEN;
                default:
            }
        }
        return "";
    }
}

package com.luqili.utils.pub.file;

public class FileDiskInfo {
    private String name;
    private long totalSize;
    private long freeSize;
    /**
     * 百分比
     */
    private int freeOf100;
    private String totalSizeName;
    private String freeSizeName;

    public int getFreeOf100() {
        return freeOf100;
    }

    public void setFreeOf100(int freeOf100) {
        this.freeOf100 = freeOf100;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(long totalSize) {
        this.totalSize = totalSize;
    }

    public long getFreeSize() {
        return freeSize;
    }

    public void setFreeSize(long freeSize) {
        this.freeSize = freeSize;
    }

    public String getTotalSizeName() {
        return totalSizeName;
    }

    public void setTotalSizeName(String totalSizeName) {
        this.totalSizeName = totalSizeName;
    }

    public String getFreeSizeName() {
        return freeSizeName;
    }

    public void setFreeSizeName(String freeSizeName) {
        this.freeSizeName = freeSizeName;
    }
}

package com.luqili.utils.pub.file;

import com.luqili.utils.pub.consver.ConvertUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class FileTotalUtils {


    /**
     * 获取系统磁盘分区文件列表
     *
     * @return
     */
    public static List<File> getOSDiskPartitionFiles() {
        List<File> files = new ArrayList<File>();
        if (SystemUtils.IS_OS_LINUX) {
            try {
                InputStream in = null;
                Scanner inscan = null;
                Process process = Runtime.getRuntime().exec("df -h");
                try {
                    // 等待命令执行完成
                    process.waitFor(10, TimeUnit.SECONDS);
                    in = process.getInputStream();
                    inscan = new Scanner(in);
                    while (inscan.hasNextLine()) {
                        String line = inscan.nextLine();
                        String[] ms = StringUtils.split(line, " ");
                        if (ms.length == 6) {
                            if (!ms[0].startsWith("/")) {
                                continue;
                            }
                            String path = ms[5];
                            if (StringUtils.isNotBlank(path) && path.startsWith("/")) {
                                File f = new File(path.trim());
                                if (f.canRead()) {
                                    files.add(f);
                                }
                            }
                        }
                    }
                } catch (InterruptedException e) {

                } finally {
                    if (inscan != null) {
                        inscan.close();
                    }
                    if (in != null) {
                        in.close();
                    }
                    if (process != null) {
                        process.destroy();
                    }
                }

            } catch (IOException e) {
                for (File file : File.listRoots()) {
                    files.add(file);
                }
            }
        } else if (SystemUtils.IS_OS_WINDOWS) {
            for (File file : File.listRoots()) {
                files.add(file);
            }
        } else {
            for (File file : File.listRoots()) {
                files.add(file);
            }
        }
        return files;
    }

    /**
     * 获取系统的磁盘分区信息 排除小于1G的分区
     *
     * @return
     */
    public static List<FileDiskInfo> getOSDiskPartitionInfos() {
        return getDiskPartitionInfo(getOSDiskPartitionFiles(), 1024 * 1024 * 1024);
    }

    /**
     * 获取文件的磁盘分区信息
     *
     * @param files
     * @param minSize
     * @return
     */
    public static List<FileDiskInfo> getDiskPartitionInfo(List<File> files, long minSize) {
        List<FileDiskInfo> infos = new ArrayList<>(files.size());
        for (File file : files) {
            long total = file.getTotalSpace();
            if (total < minSize) {
                // 排除小于1G的磁盘
                continue;
            }
            FileDiskInfo info = new FileDiskInfo();
            info.setName(file.getAbsolutePath());
            info.setTotalSize(total);
            info.setTotalSizeName(ConvertUtils.byte2FitMemorySize(total));
            long free = file.getFreeSpace();
            info.setFreeSize(free);
            info.setFreeSizeName(ConvertUtils.byte2FitMemorySize(free));
            infos.add(info);
            int val = (int) ((total - free) * 100 / total);
            info.setFreeOf100(val);
        }
        return infos;
    }

}

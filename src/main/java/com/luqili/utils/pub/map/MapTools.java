package com.luqili.utils.pub.map;

import cn.hutool.json.JSONUtil;
import cn.hutool.log.Log;

import java.util.ArrayList;
import java.util.Map;

public class MapTools {

    /**
     * 将Map内的Str对象转换为List对象
     *
     * @param map         源
     * @param elementType 转换的类型
     * @param keys        需要转换的KEY列表
     * @param <T>
     */
    public static <T> void strToList(Map map, Class<T> elementType, String... keys) {
        if (map == null) {
            return;
        }
        for (String key : keys) {
            try {
                Object v = map.get(key);
                if (v == null) {
                    map.put(key, new ArrayList<>());
                } else if (v instanceof String) {
                    map.put(key, JSONUtil.toList(v.toString(), elementType));
                }
            } catch (Exception e) {
                Log.get().error("转换：" + key + "异常，默认为空的LIST", e);
                map.put(key, new ArrayList<>());
            }
        }
    }
}

package com.luqili.utils.pub.date;

import org.apache.commons.lang3.time.DateFormatUtils;

import java.util.Date;
import java.util.Locale;

/**
 * 扩展DateFormatUtils
 *
 * @author luqili 2016年12月12日
 */
public class DateFormatUtilsExt {

    /**
     * 默认格式化样式
     * <li>yyyy-MM-dd
     *
     * @param date
     * @return
     */
    public static String formatCnStyle1(Date date) {
        return formatCNStyle(date, "yyyy-MM-dd");
    }

    /**
     * 默认格式化样式
     * <li>yyyy-MM-dd HH:mm
     *
     * @param date
     * @return
     */
    public static String formatCnStyle2(Date date) {
        return formatCNStyle(date, "yyyy-MM-dd HH:mm");
    }

    /**
     * 默认格式化样式
     * <li>yyyy-MM-dd HH:mm:ss
     *
     * @param date
     * @return
     */
    public static String formatCnStyle3(Date date) {
        return formatCNStyle(date, "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * 默认格式化样式
     * <li>yyyy年MM月dd日 HH时mm分
     *
     * @param date
     * @return
     */
    public static String formatCnStyle4(Date date) {
        return formatCNStyle(date, "yyyy年MM月dd日 HH时mm分");
    }

    /**
     * 默认格式化样式
     * <li>MM-dd HH:mm
     *
     * @param date
     * @return
     */
    public static String formatCnStyle5(Date date) {
        return formatCNStyle(date, "MM-dd HH:mm");
    }

    /**
     * 默认格式化样式
     * <li>yyyyMMddHHmm
     *
     * @param date
     * @return
     */
    public static String formatCnStyle6(Date date) {
        return formatCNStyle(date, "yyyyMMddHHmm");
    }

    /**
     * 默认格式化样式
     * <li>yyyy-MM-dd E
     * <li>如：2019-08-05 周一
     *
     * @param date
     * @return
     */
    public static String formatCnStyle10(Date date) {
        return formatCNStyle(date, "yyyy-MM-dd E");
    }

    /**
     * 默认格式化样式
     * <li>yyyy
     * <li>如：2019
     *
     * @param date
     * @return
     */
    public static String formatCnStyle20(Date date) {
        return formatCNStyle(date, "yyyy");
    }

    /**
     * 默认格式化样式
     * <li>yyyyMM
     * <li>如：201908
     *
     * @param date
     * @return
     */
    public static String formatCnStyle21(Date date) {
        return formatCNStyle(date, "yyyyMM");
    }

    /**
     * 默认格式化样式
     * <li>yyyyMMdd
     * <li>如：20190805
     *
     * @param date
     * @return
     */
    public static String formatCnStyle22(Date date) {
        return formatCNStyle(date, "yyyyMMdd");
    }

    /**
     * 默认格式化样式
     * <li>yyyyMMddHH
     * <li>如：2019080513
     *
     * @param date
     * @return
     */
    public static String formatCnStyle23(Date date) {
        return formatCNStyle(date, "yyyyMMddHH");
    }

    /**
     * 默认格式化样式
     * <li>yyyyMMddHHmm
     * <li>如：201908051326
     *
     * @param date
     * @return
     */
    public static String formatCnStyle24(Date date) {
        return formatCNStyle(date, "yyyyMMddHHmm");
    }

    /**
     * 默认格式化样式
     * <li>yyyyMMddHHmmss
     * <li>如：20190805130101
     *
     * @param date
     * @return
     */
    public static String formatCnStyle26(Date date) {
        return formatCNStyle(date, "yyyyMMddHHmmss");
    }

    /**
     * 默认格式化样式
     * <li>yyMMdd
     * <li>如：190805
     *
     * @param date
     * @return
     */
    public static String formatCnStyle27(Date date) {
        return formatCNStyle(date, "yyMMdd");
    }

    /**
     * 默认格式化样式
     * <li>yyMMddHH
     * <li>如：19080513
     *
     * @param date
     * @return
     */
    public static String formatCnStyle28(Date date) {
        return formatCNStyle(date, "yyMMddHH");
    }

    /**
     * 默认格式化样式
     * <li>HH:mm
     * <li>如：05:13
     *
     * @param date
     * @return
     */
    public static String formatCnTime(Date date) {
        return formatCNStyle(date, "HH:mm");
    }

    /**
     * 默认格式化样式
     * <li>HH:mm:ss
     * <li>如：05:13
     *
     * @param date
     * @return
     */
    public static String formatCnTimeSecond(Date date) {
        return formatCNStyle(date, "HH:mm:ss");
    }

    /**
     * 格式化样式
     *
     * @param date
     * @param style
     * @return
     */
    public static String formatCNStyle(Date date, String style) {
        return date != null ? DateFormatUtils.format(date, style, Locale.CHINA) : "";
    }

    /**
     * 格式化样式
     *
     * @param millis
     * @param style
     * @return
     */
    public static String formatCNStyle(Long millis, String style) {
        return millis != null ? DateFormatUtils.format(millis, style, Locale.CHINA) : "";
    }
}

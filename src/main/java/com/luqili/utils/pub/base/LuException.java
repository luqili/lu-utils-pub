package com.luqili.utils.pub.base;

import com.luqili.utils.pub.rest.param.LuParams;

/**
 * 基础异常类型
 *
 * @author luqili
 */
public class LuException extends RuntimeException {
    public LuException() {
        super();
    }

    public LuException(String message) {
        super(message);
    }

    public LuException(String message, Exception e) {
        super(message, e);
    }
}

package com.luqili.utils.pub.base;

/**
 * 基础异常类型 用户输入异常
 *
 * @author luqili
 */
public class LuUserInputException extends LuException {
    public LuUserInputException() {
        super();
    }

    public LuUserInputException(String message) {
        super(message);
    }
}

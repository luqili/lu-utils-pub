package com.luqili.utils.pub.district;

import java.util.HashSet;
import java.util.Set;

/**
 * 行政区划节点
 *
 * @author luqili
 */
public class DistrictNode {
    private final String name;
    private final Integer code;
    private final Integer level;
    private final Set<Integer> codes;

    public DistrictNode(Integer level, String name, Integer code, Integer... oldCodes) {
        this.level = level;
        this.name = name;
        this.code = code;
        this.codes = new HashSet<>();
        this.codes.add(code);
        if (oldCodes != null) {
            for (Integer c : oldCodes) {
                codes.add(c);
            }
        }
    }

    public String getName() {
        return name;
    }

    public Integer getCode() {
        return code;
    }

    public Integer getLevel() {
        return level;
    }

    public Set<Integer> getCodes() {
        return codes;
    }

}

package com.luqili.utils.pub.string;

import org.testng.AssertJUnit;
import org.testng.annotations.Test;

public class StringUtilsExtTest {
    @Test
    public void hideMiddleCharacter() {
        String t1 = StringUtilsExt.hideMiddleCharacter(null, 1, 2, "*", 3);
        String t2 = StringUtilsExt.hideMiddleCharacter("a", 1, 2, "*", 3);
        String t3 = StringUtilsExt.hideMiddleCharacter("ab", 1, 2, "*", 3);
        String t4 = StringUtilsExt.hideMiddleCharacter("abc", 1, 2, "*", 3);
        String t5 = StringUtilsExt.hideMiddleCharacter("abcd", 1, 2, "*", 3);
        AssertJUnit.assertEquals("***", t1);
        AssertJUnit.assertEquals("a***a", t2);
        AssertJUnit.assertEquals("a***ab", t3);
        AssertJUnit.assertEquals("a***bc", t4);
        AssertJUnit.assertEquals("a***cd", t5);
    }

    @Test
    public void startsWithStyle() {

        AssertJUnit.assertEquals(true, StringUtilsExt.startsWithStyle(null, null));
        AssertJUnit.assertEquals(true, StringUtilsExt.startsWithStyle(null, ""));
        AssertJUnit.assertEquals(true, StringUtilsExt.startsWithStyle("abcdef", ""));
        AssertJUnit.assertEquals(true, StringUtilsExt.startsWithStyle("abcdef", null));
        AssertJUnit.assertEquals(true, StringUtilsExt.startsWithStyle("abcdef", "abc"));
        AssertJUnit.assertEquals(true, StringUtilsExt.startsWithStyle("abc", "abc"));
        AssertJUnit.assertEquals(true, StringUtilsExt.startsWithStyle("abcd", "a*c"));

        AssertJUnit.assertEquals(false, StringUtilsExt.startsWithStyle(null, "abc"));
        AssertJUnit.assertEquals(false, StringUtilsExt.startsWithStyle("ab", "abc"));
        AssertJUnit.assertEquals(false, StringUtilsExt.startsWithStyle("abbc", "a*c"));
        AssertJUnit.assertEquals(false, StringUtilsExt.startsWithStyle("ABCDEF", "abc"));

    }

    @Test
    public void samePrefix() {
        AssertJUnit.assertEquals("YFT", StringUtilsExt.samePrefix("YFT123", "YFT456"));
        AssertJUnit.assertEquals("YFTQ2101", StringUtilsExt.samePrefix("YFTQ21019995", "YFTQ21012995"));
        AssertJUnit.assertEquals("", StringUtilsExt.samePrefix("ZYFTQ21019995", "YFTQ21012995"));
    }

}

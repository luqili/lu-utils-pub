package com.luqili.utils.pub.crypto;

import com.luqili.utils.pub.string.CharsetUtil;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class EncryptorsTest {

    @Test
    public void aesCrypt() {
        String src = "123456789012345612345678901234561234567890123456";
        String pwd = "1234567890123456";
        String enSrc = Encryptors.aesEncrypt(src, pwd);
        String deSrc = Encryptors.aesDecrypt(enSrc, pwd);
        assertEquals(src, deSrc);
    }

    @Test
    public void hmacSHADecrypt() {
        String src = "123456789012345612345678901234561234567890123456";
        String pwd = "1234567890123456";
        String r1 = Encryptors.hmacSHA1(src, pwd);
        String r2 = Encryptors.hmacSHA256(src, pwd);
        assertNotNull(r1);
        assertNotNull(r2);
    }

    @Test
    public void rsaEncrytAndDecrypt() {
        byte[][] rsaKeys = Encryptors.createRSAKeyPairInByte();
        String privateKey = Base64.getEncoder().encodeToString(rsaKeys[0]);
        String pubKey = Base64.getEncoder().encodeToString(rsaKeys[1]);
        String data = "这是一个测试的RSA";

        // 测试私钥加密，公钥解密
        String enStr = Encryptors.encryptByRSAByPrivateKeyToString(privateKey, data.getBytes(CharsetUtil.UTF_8));
        byte[] dataBs = Encryptors.decryptByRSAByPubKey(pubKey, enStr);
        String data1 = new String(dataBs, CharsetUtil.UTF_8);
        assertEquals(data, data1);

        // 测试公钥加密，私钥解密
        byte[] en2 = Encryptors.encryptByRSAByPubKey(rsaKeys[1], data.getBytes(CharsetUtil.UTF_8));
        byte[] dataBs2 = Encryptors.decryptByRSAByPrivateKey(rsaKeys[0], en2);
        String data2 = new String(dataBs2, CharsetUtil.UTF_8);
        assertEquals(data, data2);

    }
}

package com.luqili.utils.pub.jobs;

public class JobThreadTest {
    public static void main(String[] args) {
        final JobThread jt1 = new JobThread();
        jt1.setRunnable(new Runnable() {
            private int count = 0;

            @Override
            public void run() {
                while (jt1.jobRun) {
                    try {
                        Thread.sleep(1000);
                        count++;
                        System.out.println("当前第：" + count);
                    } catch (Exception e) {
                    }
                }

            }
        });
        jt1.start();
        jt1.forceEndJob(5000);
    }
}

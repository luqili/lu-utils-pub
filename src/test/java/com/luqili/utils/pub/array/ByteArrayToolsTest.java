package com.luqili.utils.pub.array;

import org.testng.Assert;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class ByteArrayToolsTest {


    @Test
    public void testLastIndexOf() {
        byte[] bytes = {0, 1, 2, 3, 3, 4};
        byte[] bytes2 = {3, 3, 4};
        byte[] bytes3 = {3, 3};
        Assert.assertEquals(ByteArrayTools.lastIndexOfSub(bytes, bytes2), 3);
        Assert.assertEquals(ByteArrayTools.lastIndexOfSub(bytes, bytes), 0);
        Assert.assertEquals(ByteArrayTools.lastIndexOfSub(bytes2, bytes2), 0);
        Assert.assertEquals(ByteArrayTools.lastIndexOfSub(bytes, bytes3), 3);
    }
}

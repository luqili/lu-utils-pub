package com.luqili.utils.pub.district.data;

import java.util.List;

public class GeoFeatureCollection {
    private String type;
    private List<GeoFeature> features;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<GeoFeature> getFeatures() {
        return features;
    }

    public void setFeatures(List<GeoFeature> features) {
        this.features = features;
    }

}

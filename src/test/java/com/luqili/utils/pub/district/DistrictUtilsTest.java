package com.luqili.utils.pub.district;

import org.testng.AssertJUnit;
import org.testng.annotations.Test;

public class DistrictUtilsTest {

    @Test
    public void getDistrictName() {
        String name1 = DistrictUtils.getInstance().getDistrictName(37);
        String name2 = DistrictUtils.getInstance().getDistrictName(3715);
        String name3 = DistrictUtils.getInstance().getDistrictName(371521);
        AssertJUnit.assertEquals("山东省", name1);
        AssertJUnit.assertEquals("聊城市", name2);
        AssertJUnit.assertEquals("阳谷县", name3);
    }

    @Test
    public void getDistrictNames() {
        // DistrictUtils.getDistrictNames();
    }

    @Test
    public void getDistrictFullName() {
        String fname1 = DistrictUtils.getInstance().getDistrictFullName(371521);
        String fname2 = DistrictUtils.getInstance().getDistrictFullName(110105);
        String fname3 = DistrictUtils.getInstance().getDistrictFullName(130828);
        String fname4 = DistrictUtils.getInstance().getDistrictFullName(371599);
        AssertJUnit.assertEquals("山东省聊城市阳谷县", fname1);
        AssertJUnit.assertEquals("北京市朝阳区", fname2);
        AssertJUnit.assertEquals("河北省承德市围场满族蒙古族自治县", fname3);
        AssertJUnit.assertEquals("山东省聊城市", fname4);

    }

    @Test
    public void getDistrictsByAddress() {
        String add1 = DistrictUtils.getInstance().getDistrictFullName(DistrictUtils.getInstance().getDistrictsByAddress("山东省阳谷县张秋镇"));
        String add2 = DistrictUtils.getInstance().getDistrictFullName(DistrictUtils.getInstance().getDistrictsByAddress("山东省泰安市泰山区金山路"));
        String add3 = DistrictUtils.getInstance().getDistrictFullName(DistrictUtils.getInstance().getDistrictsByAddress("山东省沂源县南麻镇许村四区"));

        AssertJUnit.assertEquals("山东省聊城市阳谷县", add1);
        AssertJUnit.assertEquals("山东省泰安市泰山区", add2);
        AssertJUnit.assertEquals("山东省淄博市沂源县", add3);
    }


}

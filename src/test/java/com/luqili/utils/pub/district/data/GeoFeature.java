package com.luqili.utils.pub.district.data;

public class GeoFeature {
    private String type;
    private GeoNode properties;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public GeoNode getProperties() {
        return properties;
    }

    public void setProperties(GeoNode properties) {
        this.properties = properties;
    }
}

package com.luqili.utils.pub.district.data;

import com.luqili.utils.pub.district.DistrictUtils;
import com.luqili.utils.pub.httpclient.HttpClientTool;
import com.luqili.utils.pub.json.JsonUtils;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

public class DistrictGetDataTest {

    private static String GeoMapUrl = "https://geo.datav.aliyun.com/areas_v2/bound/";

    public static void main(String[] args) throws IOException {
        String savePath = "/home/luqili";
        String provinceJson = HttpClientTool.requestGet(GeoMapUrl + "100000_full.json", 5000);
        FileUtils.writeStringToFile(new File(savePath + "/district/district_" + 100000 + ".json"), provinceJson, "UTF-8");

        GeoFeatureCollection provinces = JsonUtils.fromJson(provinceJson, GeoFeatureCollection.class);
        StringBuffer result = new StringBuffer();
        result.append("---START 数据来源:http://datav.aliyun.com/tools/atlas/#&lat=33.521903996156105&lng=104.29849999999999&zoom=4 ---").append("\n");
        int countP = 0;
        int countC = 0;
        int countD = 0;
        for (GeoFeature province : provinces.getFeatures()) {
            // 省
            GeoNode pn = province.getProperties();
            if ("province".equals(pn.getLevel())) {
                String urlCity = GeoMapUrl + pn.getAdcode() + "_full.json";
                String cityJson = HttpClientTool.requestGet(urlCity, 5000);
                FileUtils.writeStringToFile(new File(savePath + "/district/district_" + pn.getAdcode() + ".json"), cityJson, "UTF-8");

                GeoFeatureCollection citys = JsonUtils.fromJson(cityJson, GeoFeatureCollection.class);
                countP++;
                result.append(DistrictUtils.LEVEL_PROVINCE).append(":").append(pn.getName()).append(":").append(pn.getAdcode()).append(":").append("\n");
                for (GeoFeature cityF : citys.getFeatures()) {

                    GeoNode cn = cityF.getProperties();
                    if ("city".equals(cn.getLevel())) {
                        // 市区
                        countC++;
                        result.append(DistrictUtils.LEVEL_CITY).append(":").append(cn.getName()).append(":").append(cn.getAdcode()).append(":").append("\n");
                        // 获取 县城/城区 数据
                        String urlDistrict = GeoMapUrl + cn.getAdcode() + "_full.json";
                        String districtJson = HttpClientTool.requestGet(urlDistrict, 5000);
                        FileUtils.writeStringToFile(new File(savePath + "/district/district_" + cn.getAdcode() + ".json"), districtJson, "UTF-8");

                        try {
                            GeoFeatureCollection districts = JsonUtils.fromJson(districtJson, GeoFeatureCollection.class);
                            for (GeoFeature districtF : districts.getFeatures()) {
                                GeoNode dn = districtF.getProperties();
                                if ("district".equals(dn.getLevel())) {
                                    // 县城/城区
                                    countD++;
                                    result.append(DistrictUtils.LEVEL_DISTRICT).append(":").append(dn.getName()).append(":").append(dn.getAdcode()).append(":").append("\n");
                                }
                            }
                        } catch (Exception e) {
                            System.out.println("格式JSON异常:" + urlDistrict);
                        }

                    } else if ("district".equals(cn.getLevel())) {
                        // 县城/城区
                        countD++;
                        result.append(DistrictUtils.LEVEL_DISTRICT).append(":").append(cn.getName()).append(":").append(cn.getAdcode()).append(":").append("\n");
                    }

                }
            }
        }

        result.append("---END---").append("\n");
        result.append("---省份:" + countP + ",市:" + countC + ",城区:" + countD + "---");
        System.out.println(result);
        FileUtils.writeStringToFile(new File(savePath + "/district/district.data"), result.toString(), "UTF-8");
    }

}

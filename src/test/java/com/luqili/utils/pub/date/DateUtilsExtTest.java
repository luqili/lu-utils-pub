package com.luqili.utils.pub.date;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.testng.AssertJUnit;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.text.ParseException;
import java.util.Date;

public class DateUtilsExtTest {
    private Date d1 = null;
    private Date d2 = null;

    @BeforeMethod
    public void beforeMethod() {
        try {
            d1 = DateUtils.parseDate("2016-10-24", "yyyy-MM-dd");
            d2 = DateUtils.parseDate("2014-09-12", "yyyy-MM-dd");
        } catch (ParseException e) {
            AssertJUnit.fail("日期转换错误");
        }
    }

    @Test
    public void differ() {
        DateUtilsExt.differ(d1, d2, 1000);
    }

    @Test
    public void differDay() {
        int days = DateUtilsExt.differDay(d1, d2);
        AssertJUnit.assertEquals(773, days);
    }

    @Test
    public void differMonth() {
        int month = DateUtilsExt.differMonth(d1, d2);
        AssertJUnit.assertEquals(25, month);
    }

    @Test
    public void differYear() {
        int year = DateUtilsExt.differYear(d1, d2);
        AssertJUnit.assertEquals(2, year);
    }

    @Test
    public void getToday() {
        Date d1 = DateUtilsExt.getTodayTruncateDay();
        Date d2 = DateUtilsExt.truncateDate(new Date());
        AssertJUnit.assertEquals(d1, d2);
    }

    @Test
    public void parseDate() throws ParseException {
        String t1 = "2016-04-22";
        String t2 = "2016年04月22日";
        String t3 = "20160422";
        String t4 = "2016-04-22 12:30";
        String t5 = "2016-04-22 12:30:22";

        Date d1 = DateUtilsExt.parseDate(t1);
        Date d2 = DateUtilsExt.parseDate(t2);
        Date d3 = DateUtilsExt.parseDate(t3);
        Date d4 = DateUtilsExt.parseDate(t4);
        Date d5 = DateUtilsExt.parseDate(t5);

        Date vd1 = DateUtils.parseDate(t1, "yyyy-MM-dd");
        Date vd4 = DateUtils.parseDate(t4, "yyyy-MM-dd HH:mm");
        Date vd5 = DateUtils.parseDate(t5, "yyyy-MM-dd HH:mm:ss");

        AssertJUnit.assertEquals(vd1, d1);
        AssertJUnit.assertEquals(vd1, d2);
        AssertJUnit.assertEquals(vd1, d3);
        AssertJUnit.assertEquals(vd4, d4);
        AssertJUnit.assertEquals(vd5, d5);
    }

    @Test
    public void getPeopleAge() {

        AssertJUnit.assertEquals(8, DateUtilsExt.getPeopleAge(DateUtilsExt.parseDate("20190911"), DateUtilsExt.parseDate("20100915")));

        AssertJUnit.assertEquals(8, DateUtilsExt.getPeopleAge(DateUtilsExt.parseDate("20190909"), DateUtilsExt.parseDate("20100910")));
        AssertJUnit.assertEquals(9, DateUtilsExt.getPeopleAge(DateUtilsExt.parseDate("20190910"), DateUtilsExt.parseDate("20100910")));
        AssertJUnit.assertEquals(9, DateUtilsExt.getPeopleAge(DateUtilsExt.parseDate("20190911"), DateUtilsExt.parseDate("20100910")));

        AssertJUnit.assertEquals(8, DateUtilsExt.getPeopleAge(DateUtilsExt.parseDate("20190810"), DateUtilsExt.parseDate("20100910")));
        AssertJUnit.assertEquals(9, DateUtilsExt.getPeopleAge(DateUtilsExt.parseDate("20191010"), DateUtilsExt.parseDate("20100910")));

        AssertJUnit.assertEquals(9, DateUtilsExt.getPeopleAge(DateUtilsExt.parseDate("20190911"), DateUtilsExt.parseDate("20100215")));
        AssertJUnit.assertEquals(9, DateUtilsExt.getPeopleAge(DateUtilsExt.parseDate("20190911"), DateUtilsExt.parseDate("20100410")));

        AssertJUnit.assertEquals(9, DateUtilsExt.getPeopleAge(DateUtilsExt.parseDate("20190911"), DateUtilsExt.parseDate("20100911")));
        AssertJUnit.assertEquals(19, DateUtilsExt.getPeopleAge(DateUtilsExt.parseDate("20190911"), DateUtilsExt.parseDate("20000911")));
    }

    @Test
    public void maxDate() {
        Date d1 = DateUtilsExt.parseDate("20190911");
        Date d2 = DateUtilsExt.parseDate("20191011");
        Date d3 = DateUtilsExt.parseDate("20190911");

        AssertJUnit.assertNull(DateUtilsExt.maxDate(null, null));
        AssertJUnit.assertEquals(d1, DateUtilsExt.maxDate(d1, null));
        AssertJUnit.assertEquals(d2, DateUtilsExt.maxDate(null, d2));
        AssertJUnit.assertEquals(d2, DateUtilsExt.maxDate(d1, d2));
        AssertJUnit.assertEquals(d1, DateUtilsExt.maxDate(d1, d3));
        AssertJUnit.assertEquals(d1, d3);

    }

    @Test
    public void mergeDateTime() {
        Date d1 = DateUtilsExt.parseDateCN("20190911", "yyyyMMdd");
        Date d2 = DateUtilsExt.parseDateCN("0410", "HHmm");
        Date d12 = DateUtilsExt.mergeDateTime(d1, d2);
        String str = DateFormatUtils.format(d12, "yyyyMMddHHmm");
        AssertJUnit.assertEquals(str, "201909110410");
    }

}

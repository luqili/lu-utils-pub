package com.luqili.utils.pub.pdf;

import cn.hutool.core.collection.ListUtil;
import com.luqili.utils.pub.graphical.LuPoint2D;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType0Font;

import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PDFTest {

    public static void main(String[] args) throws IOException {
        PDDocument document = new PDDocument();
//        PDFont fontLight = PDType0Font.load(document, new File("D:/fonts/Alibaba-PuHuiTi-Light.ttf"));
        PDType0Font fontLight = PDType0Font.load(document, new File("D:/fonts/SourceHanSerifCN-VF.ttf"));
        PDFont fontBold = PDType0Font.load(document, new File("D:/fonts/Alibaba-PuHuiTi-Bold.ttf"));

        PDPage page = new PDPage(PDRectangle.A5);
        float width = (int) page.getMediaBox().getWidth();
        float height = (int) page.getMediaBox().getHeight();
        float currHeight = height;
        PDPageContentStream stream = new PDPageContentStream(document, page);

        currHeight -= 30;
        LuPdfBoxTool.writeText(stream, "writeText：测试写入文本", fontLight, 16, new LuPoint2D(0, currHeight));
        currHeight -= 30;
        LuPdfBoxTool.writeTexts(stream, ListUtil.list(true, "writeTexts:1", "writeTexts:2", "writeTexts:3"), fontLight, 16, new LuPoint2D(0, currHeight), new LuPoint2D(width, currHeight));
        currHeight -= 30;
        LuPdfBoxTool.writeTextCenter(stream, "writeTextCenter：测试题目文本居中", fontBold, 16, new LuPoint2D(0, currHeight), new LuPoint2D(width, currHeight));
        currHeight -= 10;
        LuPdfBoxTool.drawLine(stream, 0.1f, new LuPoint2D(0, currHeight), new LuPoint2D(width, currHeight));
        currHeight -= 10;
        LuPdfBoxTool.drawLine(stream, 1, new LuPoint2D(0, currHeight), new LuPoint2D(width, currHeight));
        currHeight -= 10;
        LuPdfBoxTool.drawLine(stream, 2, new LuPoint2D(0, currHeight), new LuPoint2D(width, currHeight));
        currHeight -= 10;
        LuPdfBoxTool.drawLine(stream, 3, new LuPoint2D(0, currHeight), new LuPoint2D(width, currHeight));

        currHeight -= 15;
        LuPdfBoxTool.writeText(stream, "虚线：", fontLight, 12, new LuPoint2D(0, currHeight));
        currHeight -= 10;
        LuPdfBoxTool.drawLineDotted(stream, 3, 3, new LuPoint2D(40, currHeight), new LuPoint2D(width - 40, currHeight));
        currHeight -= 10;
        LuPdfBoxTool.drawLineDotted(stream, 3, 6, new LuPoint2D(40, currHeight), new LuPoint2D(width - 40, currHeight));
        currHeight -= 10;
        LuPdfBoxTool.drawLineDotted(stream, 1, 3, new LuPoint2D(40, currHeight), new LuPoint2D(width - 40, currHeight));
        currHeight -= 10;
        LuPdfBoxTool.drawLineDotted(stream, 1, 6, new LuPoint2D(40, currHeight), new LuPoint2D(width - 40, currHeight));
        currHeight -= 10;
        LuPdfBoxTool.drawLineDotted(stream, 0, 3, new LuPoint2D(40, currHeight), new LuPoint2D(width - 40, currHeight));
        currHeight -= 10;
        LuPdfBoxTool.drawLineDotted(stream, 0, 6, new LuPoint2D(40, currHeight), new LuPoint2D(width - 40, currHeight));

        currHeight -= 15;
        List<LuPDFTextInfo> texts = new ArrayList<>();
        texts.add(new LuPDFTextInfo("红色权重3", fontLight, 15, 3, Color.RED));
        texts.add(new LuPDFTextInfo("蓝色权重6", fontLight, 15, 6, Color.BLUE));
        texts.add(new LuPDFTextInfo("黑色权重6", fontLight, 15, 6, Color.BLACK));
        texts.add(new LuPDFTextInfo("灰色权重3", fontLight, 15, 2, Color.GRAY));
        LuPdfBoxTool.writeTexts(stream, texts, new LuPoint2D(40, currHeight), new LuPoint2D(width - 40, currHeight));

        stream.close();
        document.addPage(page);
        File file = new File("D:/test/test-" + DateFormatUtils.format(new Date(), "yyyyMMddHHmmss") + ".pdf");
        document.save(new FileOutputStream(file));
        document.close();
        System.out.println("文件路径：" + file.getPath());
        Runtime.getRuntime().exec("explorer /e,/select," + file.getPath());
    }
}

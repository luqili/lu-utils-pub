package com.luqili.utils.pub.number;

import org.testng.Assert;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class HexToolsTest {

    @Test
    public void testHandHex() {
        Assert.assertEquals(HexTools.handHex("0102a", 6), "0102A0");
        Assert.assertEquals(HexTools.handHex("0102abc", 6), "0102AB");
        Assert.assertEquals(HexTools.handHex("G102abc", 6), "0102AB");
    }

    @Test
    public void testIsHexChar() {
        Assert.assertEquals(HexTools.isHexChar(null), false);
        Assert.assertEquals(HexTools.isHexChar(""), false);
        Assert.assertEquals(HexTools.isHexChar("0102a"), true);
        Assert.assertEquals(HexTools.isHexChar("0G02a"), false);
        Assert.assertEquals(HexTools.isHexChar("123acc"), true);
    }
}

package com.luqili.utils.pub.number;

import org.testng.AssertJUnit;
import org.testng.annotations.Test;

public class NumberUtilsExtTest {
    @Test
    public void parseInt() {
        AssertJUnit.assertEquals(0, NumberUtilsExt.parseInt(""));
        AssertJUnit.assertEquals(99, NumberUtilsExt.parseInt("99"));
        AssertJUnit.assertEquals(10, NumberUtilsExt.parseInt(new Long(10)));
        AssertJUnit.assertEquals(10, NumberUtilsExt.parseInt(new Short("10")));
        AssertJUnit.assertEquals(10, NumberUtilsExt.parseInt(new Integer(10)));
        AssertJUnit.assertEquals(10, NumberUtilsExt.parseInt(new Byte("10")));
        AssertJUnit.assertEquals(10, NumberUtilsExt.parseInt("0xA"));

    }

    @Test
    public void parseLong() {
        AssertJUnit.assertEquals(0L, NumberUtilsExt.parseLong(""));
        AssertJUnit.assertEquals(10L, NumberUtilsExt.parseLong(new Long(10)));
        AssertJUnit.assertEquals(10L, NumberUtilsExt.parseLong(new Short("10")));
        AssertJUnit.assertEquals(10L, NumberUtilsExt.parseLong(new Integer(10)));
        AssertJUnit.assertEquals(10L, NumberUtilsExt.parseLong(new Byte("10")));
        AssertJUnit.assertEquals(10L, NumberUtilsExt.parseLong("0xA"));

    }

    @Test
    public void parseDouble() {
        AssertJUnit.assertEquals(0D, NumberUtilsExt.parseDouble(""));
        AssertJUnit.assertEquals(10D, NumberUtilsExt.parseDouble(new Long(10)));
        AssertJUnit.assertEquals(10D, NumberUtilsExt.parseDouble(new Short("10")));
        AssertJUnit.assertEquals(10D, NumberUtilsExt.parseDouble(new Integer(10)));

        AssertJUnit.assertEquals(10.5, NumberUtilsExt.parseDouble(new Double("10.5")));
        AssertJUnit.assertEquals(10.5, NumberUtilsExt.parseDouble(new Float("10.5")));
        AssertJUnit.assertEquals(10.5, NumberUtilsExt.parseDouble("10.5"));

    }
}

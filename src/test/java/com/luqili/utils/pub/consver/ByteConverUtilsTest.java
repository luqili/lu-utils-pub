package com.luqili.utils.pub.consver;

import org.testng.Assert;
import org.testng.annotations.Test;

public class ByteConverUtilsTest {

    @Test
    public void byte2floatTest() {
        byte[] bs = new byte[4];
        bs[0] = Byte.MAX_VALUE;
        bs[1] = Byte.MIN_VALUE;
        bs[2] = 0;
        bs[3] = 5;

        float[] fs = ByteConverUtils.byte2float(bs);
        byte[] bs_1 = ByteConverUtils.float2Byte(fs);

        boolean r = bs.length == bs_1.length;
        for (int i = 0; i < fs.length; i++) {
            if (bs[i] != bs_1[i]) {
                r = false;
            }
        }
        Assert.assertEquals(true, r);
    }

    @Test
    public void float2ByteTest() {
        float[] fs = new float[5];
        fs[0] = 1.1f;
        fs[2] = Float.MIN_NORMAL;
        fs[3] = Float.MAX_VALUE;
        fs[4] = Float.MIN_VALUE;
        byte[] bs = ByteConverUtils.float2Byte(fs);
        float[] fs_1 = ByteConverUtils.byte2float(bs);
        boolean r = fs.length == fs_1.length;
        for (int i = 0; i < fs.length; i++) {
            if (fs[i] != fs_1[i]) {
                r = false;
            }
        }
        Assert.assertEquals(true, r);
    }
}
